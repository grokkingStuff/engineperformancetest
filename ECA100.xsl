<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
    <h1>ECA100 Parameters</h1>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Parameter</th>
        <th>Value</th>
        <th>Units</th>
      </tr>
      <xsl:for-each select="ECA100/Params/*">
      <tr>  
        <td>
          <xsl:choose>
			<xsl:when test= "local-name() = 'PeakCylinderPressure'">
			  <xsl:text>Peak Cylinder Pressure</xsl:text>
            </xsl:when>
		    <xsl:when test= "local-name() = 'CrankArmRadius'">
	          <xsl:text>Crank Arm Radius</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'ConRodLength'">
	          <xsl:text>Connecting Rod Length</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'CompressionRatio'">
  	          <xsl:text>Compression Ratio</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'EngineBore'">
  	          <xsl:text>Engine Bore</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'EngType'">
 	          <xsl:text>Engine Type</xsl:text>
            </xsl:when> 
	        <xsl:otherwise>
	          <xsl:value-of select="local-name()"/>
	        </xsl:otherwise>
            </xsl:choose>
        </td>
        <td><xsl:value-of select="."/></td>
        <td>
          <xsl:choose>
	        <xsl:when test= "local-name() = 'PeakCylinderPressure'">
	          <xsl:text>bar</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'Sensitivity'">
	          pC.bar<sup>-1</sup>
            </xsl:when>
	        <xsl:when test= "local-name() = 'CrankArmRadius'">
	          <xsl:text>mm</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'ConRodLength'">
	          <xsl:text>mm</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'EngineBore'">
	          <xsl:text>mm</xsl:text>
            </xsl:when>
	        <xsl:otherwise>
	          <xsl:text> </xsl:text>
	        </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      </xsl:for-each>
    </table>    

    <h1>ECA100 Cycle Data</h1>   

    <xsl:for-each select="ECA100/Data/Cycles/Cycle">
    <h2>Cycle <xsl:number value="position()" format="1" /></h2> 
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Cycle Parameter</th>
        <th>Value</th>
        <th>Units</th>
      </tr>
      <xsl:for-each select="CycleParameters/*">
      <tr>  
        <td>
          <xsl:choose>
	        <xsl:when test= "local-name() = 'IndicatedPower'">
	          <xsl:text>Indicated Power</xsl:text>
            </xsl:when>
	        <xsl:otherwise>
	          <xsl:value-of select="local-name()"/>
	        </xsl:otherwise>
          </xsl:choose>
        </td>
        <td><xsl:value-of select="."/></td>
        <td>
          <xsl:choose>
	        <xsl:when test= "local-name() = 'Speed'">
	          rev.min<sup>-1</sup>
            </xsl:when>
	        <xsl:when test= "local-name() = 'IMEP'">
	          <xsl:text>bar</xsl:text>
            </xsl:when>
	        <xsl:when test= "local-name() = 'IndicatedPower'">
	          <xsl:text>kW</xsl:text>
            </xsl:when>
	        <xsl:otherwise>
	          <xsl:text> </xsl:text>
	        </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      </xsl:for-each>
    </table> 


      <xsl:for-each select="Revs/Revolution">
        <h3>Revolution <xsl:number value="position()" format="1" /></h3> 	
        <table border="1">
          <tr bgcolor="#9acd32">
            <th>Angle (degrees)</th>
            <th>Volume (cm<sup>3</sup>)</th>
            <th>Pressure (bar)</th>
            <th>Voltage (V)</th>
          </tr>	  
          <xsl:for-each select="Reading">
          <tr>  
            <td><xsl:value-of select="Angle"/></td>
            <td><xsl:value-of select="Volume"/></td>
            <td><xsl:value-of select="Pressure"/></td>
            <td><xsl:value-of select="Voltage"/></td>
          </tr>
          </xsl:for-each>  
	  </table>
      </xsl:for-each>  

    </xsl:for-each>    
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
